# -*- coding: utf-8 -*-

{
    "name":  "Manage Student",
    "summary":  "Management information",
    "category":  "Productivity",
    "version":  "1.0.0",
    "sequence":  1,
    "author":  "Ngocnhi",
    "website":  "https://mocdiep.com",
    "description":  """Learn create""",
    "depends":  ['analytic'],
    "data":  [
        'security/ir.model.access.csv',
        'views/student_view.xml',
    ],
    "images":  ['static/description/Banner.png'],
    "application":  True,
    "installable":  True,
    "auto_install":  False,
}
