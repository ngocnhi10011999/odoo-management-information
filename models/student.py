from odoo import api, fields, models, _


class Student(models.Model):

    _name = "student.informations"
    _description = "Student's informations"

    name = fields.Char('Name', required=True)
    student_id = fields.Integer('Student ID', required=True)
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
        ], required=True, default='other')
    phone = fields.Char('Phone Number', required=False)
    email = fields.Char('Email')
    birth = fields.Date('Birthday', required=True)
    address = fields.Char('Address', required=True)
    note = fields.Char('Note')
